﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace oblig3.Model
{
    public class Character
    {
        /// <summary>
        /// Properties and relationship set for the Character model
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CharacterId { get; set; }

        [Column("FullName")]
        [StringLength(30, ErrorMessage = "Name cannot be longer than 30 characters.")]
        public string FullName { get; set; }

        [Column("Alias")]
        [StringLength(25, ErrorMessage = "Alias cannot be longer than 25 characters.")]
        public string? Alias { get; set; }

        [Column("Gender")]
        [StringLength(10, ErrorMessage = "Gender cannot be longer than 10 characters.")]
        public string? Gender { get; set; }
        [Url]
        public string? Picture { get; set; }
        public ICollection<Movie> Movies { get; set; }
    }
}
