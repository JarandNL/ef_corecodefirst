﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace oblig3.Model
{
    public class Franchise
    {
        /// <summary>
        /// Properties and relationship set for the Franchise model
        /// </summary>

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int FranchiseId { get; set; }

        [Column("Name")]
        [StringLength(20, ErrorMessage = "Name can not be longer then 20 characters.")]
        public string Name { get; set; }

        [Column("Description")]
        [StringLength(255, ErrorMessage = "Description can not be longer then 255 characters.")]
        public string Description { get; set; }


        public ICollection<Movie>? Movies { get; set; }

    }
}
