﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Configuration;

namespace oblig3.Model
{
    public class Movie
    {
        /// <summary>
        /// Properties and relationship set for the Movie model
        /// </summary>

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MovieId { get; set; }

        [Column("Title")]
        [StringLength(30, ErrorMessage = "Title can not be longer then 30 characters.")]
        public string Title { get; set; }

        [Column("Genre")]
        [StringLength(30, ErrorMessage = "Genre can not be longer then 30 characters.")]
        public string Genre { get; set; }

        [Column("ReleaseYear")]
        [IntegerValidator(MinValue = 1888, MaxValue = 2023)]
        public int? Releaseyear { get; set; }

        [Column]
        [StringLength(25, ErrorMessage = "Director cannot be longer than 25 characters.")]
        public string? Director { get; set; }

        [Url]
        [DisplayName("Url to picture")]
        public string? Picture { get; set; }

        [Url]
        [DisplayName("Imdb link")]
        public string? Trailer { get; set; }

        public int? FranchiseId { get; set; }
        public virtual ICollection<Character> Characters { get; set; }
    }
}
