﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using oblig3.Data;
using oblig3.DataTransferObject.MovieDTO;
using oblig3.Model;

namespace oblig3.Services
{
    public class FranchiseService : IFranchiseService
    {
        private readonly CharacterDbContext _context;

        public FranchiseService(CharacterDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Methode to Get all Franchises
        /// </summary>
        /// <returns>All Franchises</returns>
        public async Task<IEnumerable<Franchise>> GetAllFranchises()
        {
            return await _context.Franchises.ToListAsync();

        }

        /// <summary>
        /// Methode to get a Franchise by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Franchise a spesific Franchise</returns>
        public async Task<Franchise> GetFranchiseById(int id)
        {
            return await _context.Franchises.FindAsync(id);
        }

        /// <summary>
        /// Methode to add a new Franchise
        /// </summary>
        /// <param name="franchise"></param>
        /// <returns>New Franchise</returns>
        public async Task<Franchise> AddFranchise(Franchise franchise)
        {
            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();
            return franchise;
        }

        /// <summary>
        /// Methode to delete a Franchise
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Deletes a Franchise</returns>
        public async Task DeleteFranchise(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Methode to check if a spesific Franchise Exist
        /// </summary>
        /// <param name="id"></param>
        /// <returns>true or false</returns>
        public bool FranchiseExist(int id)
        {
            return _context.Franchises.Any(e => e.FranchiseId == id);
        }

        /// <summary>
        /// Methode for update a spesific Franchise
        /// </summary>
        /// <param name="franchise"></param>
        /// <returns>Updated Franchise</returns>
        public async Task UpdateFranchise(Franchise franchise)
        {
            _context.Entry(franchise).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Method to get all movies in a spesific Franchise
        /// </summary>
        /// <param name="id"></param>
        /// <returns>All movies in a spesific Franchise</returns>
        public async Task<List<Franchise>> MoviesInFranchise(int id)
        {
            return await _context.Franchises
                .Where(f => f.FranchiseId == id)
                .Select(f => new Franchise
                {
                    Movies = f.Movies
                })
                .ToListAsync();
        }

        /// <summary>
        /// Methode to get all Characters in a spesific Franchise
        /// </summary>
        /// <param name="id"></param>
        /// <returns>All Characters in a spesific Franchise</returns>
        public async Task<List<Franchise>> CharactersInFranchise(int id)
        {
            return await _context.Franchises
                .Include(f => f.Movies)
                .ThenInclude(f => f.Characters)
                .Where(f => f.FranchiseId == id)
                .ToListAsync();
        }
        public async Task UpdateMovieFranchise(int id, List<int> movie)
        {
            Franchise franchise = await _context.Franchises
                .Include(f => f.Movies)
                .Where(f => f.FranchiseId == id)
                .FirstAsync();
            List<Movie> movies = new();
            foreach (int moviesId in movie)
            {
                Movie _movie = await _context.Movies.FindAsync(moviesId);
                if(movie == null)
                {
                    throw new KeyNotFoundException();
                }
                movies.Add(_movie);
            }
            franchise.Movies = movies;
            await _context.SaveChangesAsync();
        }
    }
}
