﻿using oblig3.Model;

namespace oblig3.Services
{
    /// <summary>
    /// Creating Interface for Character Service
    /// </summary>
    public interface ICharacterService
    {
        public Task<IEnumerable<Character>> GetAllCharacters(); // Get all characters
        public Task<Character> GetCharacterById(int id); //Get specific character by id
        public Task<Character> AddCharacter(Character character); //Create a character
        public Task DeleteCharacter(int id); //Delete specific character by id
        public bool CharacterExist(int id); //Checks if an existing character exist
        public Task UpdateCharacter(Character character); //Updates an exisiting character
    }
}
