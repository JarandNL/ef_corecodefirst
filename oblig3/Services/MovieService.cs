﻿using Microsoft.EntityFrameworkCore;
using oblig3.Data;
using oblig3.Model;

namespace oblig3.Services
{
    public class MovieService : IMovieService
    {
        private readonly CharacterDbContext _context;
        public MovieService(CharacterDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Methode to get all Movies
        /// </summary>
        /// <returns>All Movies</returns>
        public async Task<IEnumerable<Movie>> GetAllMovies()
        {
            return await _context.Movies.ToListAsync();

        }

        /// <summary>
        /// Methode to get a spesific movie by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>a spesific movie</returns>
        public async Task<Movie> GetMovieById(int id)
        {
            return await _context.Movies.FindAsync(id);         
        }

        /// <summary>
        /// Methode to add a new movie
        /// </summary>
        /// <param name="movie"></param>
        /// <returns>A new Movie</returns>
        public async Task<Movie> AddMovie(Movie movie)
        {
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();
            return movie;
        }

        /// <summary>
        /// Methode to delete a Movie
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Deletes a </returns>
        public async Task DeleteMovie(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Methode to check if a spesific Movie Exist
        /// </summary>
        /// <param name="id"></param>
        /// <returns>True or False</returns>
        public bool MovieExist(int id)
        {
            return _context.Movies.Any(e => e.MovieId == id);
        }

        /// <summary>
        /// Methode to Update a spesific movie
        /// </summary>
        /// <param name="movie"></param>
        /// <returns>Updated Movie</returns>
        public async Task UpdateMovie(Movie movie)
        {
            _context.Entry(movie).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Methode to get all Characters in Movie
        /// </summary>
        /// <param name="id"></param>
        /// <returns>All Characters in Movie</returns>
        public async Task<List<Movie>> CharactersInMovie(int id)
        {
            return await _context.Movies
                .Where(c => c.MovieId == id)
                .Select(c => new Movie
                {
                    Characters = c.Characters
                })
                .ToListAsync();
        }

        /// <summary>
        /// methode to Update a spesific Character in Movie
        /// </summary>
        /// <param name="id"></param>
        /// <param name="characters"></param>
        /// <returns>Updated Character</returns>
        /// <exception cref="KeyNotFoundException"></exception>
        public async Task UpdateMovieCharacters(int id, List<int> characters)
        {
            Movie movie = await _context.Movies
                .Include(m => m.Characters)
                .Where(m => m.MovieId == id)
                .FirstAsync();
            List<Character> chars = new();
            foreach (int characterId in characters)
            {
                Character character = await _context.Characters.FindAsync(characterId);
                if(character == null)
                {
                    throw new KeyNotFoundException();
                }
                chars.Add(character);
            }
            movie.Characters = chars;
            await _context.SaveChangesAsync();
        }
    }
}
