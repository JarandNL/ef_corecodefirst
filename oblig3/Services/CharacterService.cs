﻿using Microsoft.EntityFrameworkCore;
using oblig3.Data;
using oblig3.Model;
//using static oblig3.Services.CharacterService;

namespace oblig3.Services
{
    public class CharacterService : ICharacterService
    {
        private readonly CharacterDbContext _context;

        public CharacterService(CharacterDbContext context)
        {
            _context = context;
        }
        /// <summary>
        /// Methode to get all Characters
        /// </summary>
        /// <returns>all characters</returns>

        public async Task<IEnumerable<Character>> GetAllCharacters()
        {
            return await _context.Characters.ToListAsync();

        }
        /// <summary>
        /// Methode to get Character by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns a spesific Character</returns>

        public async Task<Character> GetCharacterById(int id)
        {
            return await _context.Characters.FindAsync(id);
        }

        /// <summary>
        /// Methode to Add a new Character
        /// </summary>
        /// <param name="character"></param>
        /// <returns>New Character</returns>
        public async Task<Character> AddCharacter(Character character)
        {
            _context.Characters.Add(character);
            await _context.SaveChangesAsync();
            return character;
        }

        /// <summary>
        /// Methode to deleta a Character
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Deletes a Character</returns>
        public async Task DeleteCharacter(int id)
        {
            var character = await _context.Characters.FindAsync(id);
            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();
        }
        public bool CharacterExist(int id)
        {
            return _context.Characters.Any(e => e.CharacterId == id);
        }

        /// <summary>
        /// Methode to update a spesific Character
        /// </summary>
        /// <param name="character"></param>
        /// <returns>Updated Character</returns>
        public async Task UpdateCharacter(Character character)
        {
            _context.Entry(character).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
    }
}
