﻿using Microsoft.AspNetCore.Mvc;
using oblig3.DataTransferObject.MovieDTO;
using oblig3.Model;

namespace oblig3.Services
{
    /// <summary>
    /// Creating Interface for Franchise Service
    /// </summary>
    public interface IFranchiseService
    {
        public Task<IEnumerable<Franchise>> GetAllFranchises(); // Get all Franchises
        public Task<Franchise> GetFranchiseById(int id); //Get specific franchise by id
        public Task<Franchise> AddFranchise(Franchise franchise); //Create a franchise
        public Task DeleteFranchise(int id); //Delete specific franchise by id
        public bool FranchiseExist(int id); //Checks if an existing franchise exist
        public Task UpdateFranchise(Franchise franchise); //Updates an franchise
        public Task<List<Franchise>> MoviesInFranchise(int id); //Gets all movies in a franchise
        public Task<List<Franchise>> CharactersInFranchise(int id); //Gets all characters in moves that are in a franchise
        public Task UpdateMovieFranchise(int id, List<int> movie); //Updates the franchise of a movie
    }
}
