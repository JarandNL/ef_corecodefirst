﻿using oblig3.Model;

namespace oblig3.Services
{
    /// <summary>
    /// Creating interface for movie service
    /// </summary>
    public interface IMovieService
    {
        public Task<IEnumerable<Movie>> GetAllMovies(); // Get all Movies
        public Task<Movie> GetMovieById(int id); //Get specific movie by id
        public Task<Movie> AddMovie(Movie movie); //Create a movie
        public Task DeleteMovie(int id); //Delete specific movie by id
        public bool MovieExist(int id); //Checks if an existing movie exist
        public Task UpdateMovie(Movie movie); //Updates an exisiting movie
        public Task<List<Movie>> CharactersInMovie(int id);
        public Task UpdateMovieCharacters(int id, List<int> characters);

        /* Denne trengs:
        public Task GetCharactersInMovie(int id); //Gets all characters in a movie
        */
    }
}
