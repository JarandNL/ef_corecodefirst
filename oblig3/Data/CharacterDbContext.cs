﻿using Microsoft.EntityFrameworkCore;
using oblig3.Model;

namespace oblig3.Data
{
    public class CharacterDbContext : DbContext
    {
        public CharacterDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Character> Characters { get; set; }
        public DbSet<Franchise> Franchises { get; set; }
        public DbSet<Movie> Movies { get; set; }

        /// <summary>
        /// Methode to reccive data from SeedData.cs and adding data into CharacterMovieTable
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {


            modelBuilder.Entity<Character>().HasData(DataSeeder.GetCharacters());
            modelBuilder.Entity<Franchise>().HasData(DataSeeder.GetFranchises());
            modelBuilder.Entity<Movie>().HasData(DataSeeder.GetMovies());

            modelBuilder.Entity<Movie>()
                .HasMany(p => p.Characters)
                .WithMany(m => m.Movies)
                .UsingEntity<Dictionary<string, string>>("CharacterMovie",
                r => r.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                l => l.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                je =>
                {
                    je.HasKey("MovieId", "CharacterId");
                    je.HasData(
                        new { MovieId = 1, CharacterId = 1 },
                        new { MovieId = 1, CharacterId = 3 },
                        new { MovieId = 2, CharacterId = 2 },
                        new { MovieId = 3, CharacterId = 3 } 
                        );
                });

        }
    }
}
