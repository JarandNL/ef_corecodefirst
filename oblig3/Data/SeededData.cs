﻿using oblig3.Model;

namespace oblig3.Data
{
    /// <summary>
    /// Class contains methods to get movies, characters and franchise.
    /// </summary>
    /// <returns>New movies, characters and franchises</returns>
    /// 
    public class DataSeeder
    {
        //// <summary>
        /// Methode to Create Caracthers 
        /// </summary>
        /// <returns>Characters</returns>
        public static List<Character> GetCharacters()
        {
            List<Character> createCharacters = new List<Character>();
            createCharacters.Add(new Character()
            {
                CharacterId = 1,
                FullName = "Jarand Larsen",
                Alias = "Utvikler",
                Gender = "Male",
                Picture = "",

            });
            createCharacters.Add(new Character()
            {
                CharacterId = 2,
                FullName = "Oda Spade",
                Alias = "Alias",
                Gender = "Female",
                Picture = ""
            });
            createCharacters.Add(new Character()
            {
                CharacterId = 3,
                FullName = "Kjetil Rønhovde",
                Alias = "Utvikler",
                Gender = "Male",
                Picture = ""
            });
            return createCharacters;
        }

        /// <summary>
        /// Methode to Create Franchises
        /// </summary>
        /// <returns>Create Franchises</returns>
        public static List<Franchise> GetFranchises()
        {
            List<Franchise> createFranchises = new List<Franchise>();
            createFranchises.Add(new Franchise()
            {
                FranchiseId = 1,
                Name = "test1",
                Description = "ingen description"
            });
            createFranchises.Add(new Franchise()
            {
                FranchiseId = 2,
                Name = "test2",
                Description = "dette er bra"
            });
            createFranchises.Add(new Franchise()
            {
                FranchiseId = 3,
                Name = "test3",
                Description = ""
            });
            return createFranchises;
        }

        /// <summary>
        /// Methode to Create Movies
        /// </summary>
        /// <returns>Create Movies</returns>
        public static List<Movie> GetMovies()
        {
            List<Movie> movies = new List<Movie>()
            {
                new Movie()
                {
                    MovieId = 1,
                    Title = "The amazing Spiderman 1",
                    Genre = "Action, Fantasy",
                    Releaseyear = 2012,
                    Director = "Sam Raimi",
                    Picture = "https://www.imdb.com/title/tt0948470/mediaviewer/rm2110040832/?ref_=tt_ov_i",
                    Trailer = "https://www.imdb.com/video/vi717595161/?playlistId=tt0948470&ref_=tt_ov_vi",
                    FranchiseId = 1,
                },

                new Movie()
                {
                    MovieId = 2,
                    Title = "The amazing Spiderman2",
                    Genre = "Action, Fantasy",
                    Releaseyear = 2014,
                    Director = "Sam Raimi",
                    Picture = "https://www.imdb.com/title/tt1872181/mediaviewer/rm2573584384/?ref_=tt_ov_i",
                    Trailer = "https://www.imdb.com/video/vi694921753/?playlistId=tt1872181&ref_=tt_ov_vi",
                    FranchiseId = 2,
                },
                new Movie()
                {
                    MovieId = 3,
                    Title = "The amazing Spiderman3",
                    Genre = "Comedy",
                    Releaseyear = 2023,
                    Director = "Sam Raimi",
                    Picture = "https://www.imdb.com/title/tt0145487/mediaviewer/rm3632146944/?ref_=tt_ov_i",
                    Trailer = "https://www.imdb.com/video/vi1376109081/?playlistId=tt0145487&ref_=tt_pr_ov_vi",
                    FranchiseId = 3,
                }
            };
            return movies;
        }
    }
}

