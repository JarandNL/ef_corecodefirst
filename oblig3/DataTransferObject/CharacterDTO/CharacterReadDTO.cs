﻿namespace oblig3.DataTransferObject.CharacterDTO
{
    public class CharacterReadDTO
    {
        /// <summary>
        /// properties for reading a dto character
        /// </summary>
        public int CharacterId { get; set; }
        public string FullName { get; set; }
        public string Gender { get; set; }
        public List<int> Movies { get; set; }
    }
}
