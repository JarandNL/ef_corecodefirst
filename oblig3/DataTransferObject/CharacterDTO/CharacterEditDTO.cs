﻿namespace oblig3.DataTransferObject.CharacterDTO
{
    public class CharacterEditDTO
    {
        /// <summary>
        /// Properties for editing a dto character
        /// </summary>
        public int CharacterId { get; set; }
        public string FullName { get; set; }
        public string Gender { get; set; }
    }
}
