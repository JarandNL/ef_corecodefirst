﻿namespace oblig3.DataTransferObject.CharacterDTO
{
    public class CharacterCreateDTO
    {
        /// <summary>
        /// Properties for create character DTO
        /// </summary>
        public string? FullName { get; set; }
        public string? Gender { get; set; }
    }
}
