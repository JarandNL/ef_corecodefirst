﻿namespace oblig3.DataTransferObject.FranchiseDTO
{
    public class FranchiseMovieDTO
    {
        /// <summary>
        /// Data transfer object to get all movies in a franchise
        /// </summary>
        public List<int>? Movies { get; set; }
    }
}
