﻿namespace oblig3.DataTransferObject.FranchiseDTO
{
    public class FranchiseCharacterDTO
    {
        /// <summary>
        /// Data transfer object to get all characters in a franchise.
        /// </summary>
        public List<int> Characters { get; set; }
    }
}
