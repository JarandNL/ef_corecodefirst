﻿namespace oblig3.DataTransferObject.FranchiseDTO
{
    public class FranchiseReadDTO
    {
        /// <summary>
        /// Data transfer Object to Read FranchiseId, Name, Description and Movies in a Franchise
        /// </summary>
        public int FranchiseId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<int>? Movies { get; set; }
    }
}
