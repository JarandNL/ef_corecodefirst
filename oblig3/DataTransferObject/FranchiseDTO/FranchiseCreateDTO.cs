﻿namespace oblig3.DataTransferObject.FranchiseDTO
{
    public class FranchiseCreateDTO
    {
        /// <summary>
        /// Data transfer Object to Create Name and Description in a Franchise
        /// </summary>
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
