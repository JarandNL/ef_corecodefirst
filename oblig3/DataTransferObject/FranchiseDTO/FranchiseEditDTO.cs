﻿namespace oblig3.DataTransferObject.FranchiseDTO
{
    public class FranchiseEditDTO
    {
        /// <summary>
        /// Data transfer Object to Edit FranchiseId, Name and Description in a Franchise
        /// </summary>
        public int FranchiseId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
