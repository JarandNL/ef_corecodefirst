﻿using oblig3.Model;

namespace oblig3.DataTransferObject.MovieDTO
{
    public class MovieReadDTO
    {
        /// <summary>
        /// Data transfer object to Read MovieId, Title, Releaseyear, Genre, Director, Characters And FranchisId in a movie.
        /// </summary>
        public int MovieId { get; set; }
        public string Title { get; set; }
        public int Releaseyear { get; set; }
        public string Genre { get; set; }
        public string Director { get; set; }
        public virtual ICollection<Character> Characters { get; set; }
        public int FranchiseId { get; set; }
    }
}
