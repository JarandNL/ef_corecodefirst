﻿namespace oblig3.DataTransferObject.MovieDTO
{
    public class MovieEditDTO
    {
        //// <summary>
        /// Data transfer object to Edit MoviId, Title, Genre, Releaseyear, Director, Picture, Trailer And Franchise in a movie.
        /// </summary>
        public int MovieId { get; set; }
        public string Title { get; set; }
        public string Genre { get; set; }
        public int Releaseyear { get; set; }
        public string Director { get; set; }
        public string Picture { get; set; }
        public string Trailer { get; set; }
        public int Franchise { get; set; }
    }
}
