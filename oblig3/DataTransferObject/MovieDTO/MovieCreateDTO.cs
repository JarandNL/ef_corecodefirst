﻿namespace oblig3.DataTransferObject.MovieDTO
{
    public class MovieCreateDTO
    {
        /// <summary>
        /// Data transfer object to Create Title, Genre, Releaseyear, Director And FranchiseId in a movie.
        /// </summary>
        public string Title { get; set; }
        public string Genre { get; set; }
        public int? Releaseyear { get; set; }
        public string? Director { get; set; }
        public int? FranchiseId { get; set; }

    }
}
