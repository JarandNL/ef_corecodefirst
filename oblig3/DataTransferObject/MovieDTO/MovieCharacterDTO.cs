﻿namespace oblig3.DataTransferObject.MovieDTO
{
    public class MovieCharacterDTO
    {
        /// <summary>
        /// Data transfer object to get all characters in a movie.
        /// </summary>
        public List<int> Characters { get; set; }
    }
}
