using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using oblig3.Data;
using oblig3.Services;
using System.Reflection;

namespace oblig3
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.

            builder.Services.AddAutoMapper(typeof(Program));
            builder.Services.AddDbContext<CharacterDbContext>(options =>
            options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));


            builder.Services.AddControllers();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();
            builder.Services.AddScoped(typeof(ICharacterService), typeof(CharacterService));
            builder.Services.AddScoped(typeof(IMovieService), typeof(MovieService));
            builder.Services.AddScoped(typeof(IFranchiseService), typeof(FranchiseService));

            builder.Services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "EF_CoreCodeFirst | Assignment 3",
                    Version = "v1",
                    Description = "An assignment made by Jarand and Kjetil",
                    Contact = new OpenApiContact
                    {
                        Name = "Jarand & Kjetil",
                        Email = "jarand.larsen@no.experis.com & kjetil.ronhovde@no.experis.com",
                    },
                });
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });


            var app = builder.Build();



            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            app.UseAuthorization();


            app.MapControllers();

            app.Run();
        }
    }
}