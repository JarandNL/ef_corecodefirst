﻿using AutoMapper;
using oblig3.DataTransferObject.CharacterDTO;
using oblig3.Model;

namespace oblig3.Profiles
{
    public class CharacterProfile : Profile
    {
        /// <summary>
        /// Method to map character model to DTO classes
        /// </summary>
        public CharacterProfile()
        {
            CreateMap<Character, CharacterReadDTO>()
                .ForMember(cdto => cdto.Movies, opt => opt
                .MapFrom(c => c.Movies.Select(c => c.MovieId).ToArray()))
                .ReverseMap();
            CreateMap<Character, CharacterCreateDTO>()
                .ReverseMap();
            CreateMap<CharacterEditDTO, Character>()
                .ReverseMap();
        }
    }
}
