﻿using AutoMapper;
using oblig3.DataTransferObject.MovieDTO;
using oblig3.Model;

namespace oblig3.Profiles
{
    public class MovieProfile : Profile
    {
        /// <summary>
        /// Method to map Movie model to DTO classes
        /// </summary>
        public MovieProfile()
        {
            CreateMap<Movie, MovieReadDTO>()
                .ForMember(mdto => mdto.Characters, opt => opt
                .MapFrom(m => m.Characters.Select(m => m.CharacterId).ToArray()));
            CreateMap<Movie, MovieCreateDTO>()
                .ReverseMap();
            CreateMap<Movie, MovieEditDTO>()
                .ReverseMap();
            CreateMap<Movie, MovieCharacterDTO>()
                .ForMember(mdto => mdto.Characters, opt => opt
                .MapFrom(m => m.Characters.Select(m=>m.CharacterId).ToArray()));
        }
    }
}
