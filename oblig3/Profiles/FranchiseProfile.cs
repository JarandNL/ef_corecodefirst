﻿using AutoMapper;
using oblig3.Model;
using oblig3.DataTransferObject.FranchiseDTO;

namespace oblig3.Profiles
{
    public class FranchiseProfile : Profile
    {
        /// <summary>
        /// Method to map Franchise model to DTO classes
        /// </summary>
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseReadDTO>()
                .ForMember(fdto => fdto.Movies, opt => opt
                .MapFrom(f => f.Movies.Select(f => f.MovieId).ToArray()));
            CreateMap<FranchiseCreateDTO, Franchise>()
                .ReverseMap();
            CreateMap<FranchiseEditDTO, Franchise>()
                .ReverseMap();
            CreateMap<Franchise, FranchiseMovieDTO>()
                .ForMember(fdto => fdto.Movies, opt => opt
                .MapFrom(f => f.Movies.Select(f => f.MovieId).ToArray()));
            CreateMap<Franchise, FranchiseCharacterDTO>()
                .ForMember(fdto => fdto.Characters, opt => opt
                .MapFrom(f => f.Movies.Select(f => f.Characters).ToArray()));
        }
    }
}
