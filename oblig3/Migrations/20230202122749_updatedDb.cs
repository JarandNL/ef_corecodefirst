﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace oblig3.Migrations
{
    public partial class updatedDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Characters_Franchises_FranchiseId",
                table: "Characters");

            migrationBuilder.DropIndex(
                name: "IX_Characters_FranchiseId",
                table: "Characters");

            migrationBuilder.DropColumn(
                name: "FranchiseId",
                table: "Characters");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "FranchiseId",
                table: "Characters",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Characters_FranchiseId",
                table: "Characters",
                column: "FranchiseId");

            migrationBuilder.AddForeignKey(
                name: "FK_Characters_Franchises_FranchiseId",
                table: "Characters",
                column: "FranchiseId",
                principalTable: "Franchises",
                principalColumn: "FranchiseId");
        }
    }
}
