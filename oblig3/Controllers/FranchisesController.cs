﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NuGet.ProjectModel;
using oblig3.Data;
using oblig3.DataTransferObject.CharacterDTO;
using oblig3.DataTransferObject.FranchiseDTO;
using oblig3.DataTransferObject.MovieDTO;
using oblig3.Model;
using oblig3.Services;

namespace oblig3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("Application/json")]
    [Consumes("Application/json")]

    public class FranchisesController : ControllerBase
    {
        private readonly IFranchiseService _franchiseService;
        private readonly IMapper _mapper;
        

        public FranchisesController(IFranchiseService franchiseService, IMapper mapper)
        {
            _franchiseService = franchiseService;
            _mapper = mapper;
        }
        /// <summary>
        /// Method to get all franchises
        /// </summary>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]

        
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises()
        {
            return _mapper.Map<List<FranchiseReadDTO>>(await _franchiseService.GetAllFranchises());
        }

        
        /// <summary>
        /// Methode to Get a spesific Franchise by Id in Swagger
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A speisfic Franchise in Swagger</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchise(int id)
        {
            Franchise franchise = await _franchiseService.GetFranchiseById(id);

            if (franchise == null)
            {
                return NotFound();
            }

            return _mapper.Map<FranchiseReadDTO>(franchise);
        }

        
        /// <summary>
        /// Methode to Update a Franchise in Swagger 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="FranchiseDTO"></param>
        /// <returns>A Updated Franchise in Swagger</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, FranchiseEditDTO FranchiseDTO)
        {
            if (id != FranchiseDTO.FranchiseId)
            {
                return BadRequest();
            }

            if (!_franchiseService.FranchiseExist(id))
            {
                return NotFound();
            }

            Franchise modelFranchise = _mapper.Map<Franchise>(FranchiseDTO);
            await _franchiseService.UpdateFranchise(modelFranchise);

            return NoContent();
        }

        /// <summary>
        /// Methode to add a new Franchise in Swagger
        /// </summary>
        /// <param name="FranchiseDTO"></param>
        /// <returns>Adding a new Franchise in Swagger</returns>
        [HttpPost]
        public async Task<ActionResult<Franchise>> PostFranchise(FranchiseCreateDTO FranchiseDTO)
        {
            var modelFranchise = _mapper.Map<Franchise>(FranchiseDTO);
            modelFranchise = await _franchiseService.AddFranchise(modelFranchise);
            return CreatedAtAction("GetFranchise",
                new { id = modelFranchise.FranchiseId },
                _mapper.Map<FranchiseReadDTO>(modelFranchise));

        }

        
        /// <summary>
        /// Methode to delete a spesific Franchise in Swagger
        /// </summary>
        /// <param name="id"></param>
        /// <returns> A deleted Franchise in Swagger</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            if (!_franchiseService.FranchiseExist(id))
            {
                return NotFound();
            }
            await _franchiseService.DeleteFranchise(id);
            return NoContent();
        }

        /// <summary>
        /// Methode to get all the Movies in a spesific Franchise by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>All the Movies in a spesific Franchise </returns>
        [HttpGet("getMovies/{id}")]
        public async Task<ActionResult<List<FranchiseMovieDTO>>> GetMoviesByFranchise(int id)
        {
            List<Franchise> franchise = await _franchiseService.MoviesInFranchise(id);
            if (franchise == null)
            {
                return NotFound();
            }
            var franchiseReadMovies = _mapper.Map<List<FranchiseMovieDTO>>(franchise);
            return franchiseReadMovies;
        }

        /// <summary>
        /// This method has some mapping issues, and is not working as intended
        /// </summary>
        /// <param name="id"></param>
        /// <returns>error 500 mapping error.</returns>
        [HttpGet("getcharacters/{id}")]
        public async Task<ActionResult<List<FranchiseCharacterDTO>>> GetCharactersByFranchise(int id)
        {
            List<Franchise> franchise = await _franchiseService.CharactersInFranchise(id);
            if (franchise == null)
            {
                return NotFound();
            }
            var franchiseReadCharacters = _mapper.Map<List<FranchiseCharacterDTO>>(franchise);
            return franchiseReadCharacters;
        }
        /// <summary>
        /// Method that updates the movies in a specific franchise.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movie"></param>
        /// <returns>Either success, Not found or Invalid movie</returns>
        [HttpPut("getmovies/{id}")]
        public async Task<IActionResult> UpdateMoviesInFranchise(int id, List<int> movie)
        {
            if (!_franchiseService.FranchiseExist(id))
            {
                return NotFound();
            }
            try
            {
                await _franchiseService.UpdateMovieFranchise(id, movie);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Invalid movie");
            }
            return NoContent();
        }
    }
}
    

