﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using oblig3.Data;
using oblig3.DataTransferObject.CharacterDTO;
using oblig3.DataTransferObject.MovieDTO;
using oblig3.Model;
using oblig3.Services;

namespace oblig3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("Application/json")]
    [Consumes("Application/json")]
    public class MoviesController : ControllerBase
    {
        private readonly IMovieService _movieService;
        private readonly IMapper _mapper;

        public MoviesController(IMovieService movieService, IMapper mapper)
        {
            _movieService = movieService;
            _mapper = mapper;
        }
        /// <summary>
        /// Method to get all movies
        /// </summary>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        
        
        
        [HttpGet]
                 
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies()
        {
            return _mapper.Map<List<MovieReadDTO>>(await _movieService.GetAllMovies());
        }

        /// <summary>
        /// Methode to get a spesific Movie in Swagger
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A spesific Movie in Swagger</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovie(int id)
        {
            Movie movie = await _movieService.GetMovieById(id);

            if (movie == null)
            {
                return NotFound();
            }

            return _mapper.Map<MovieReadDTO>(movie);
        }

        
        /// <summary>
        /// Methode to update a spesific Movie by Id in Swagger
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movieDTO"></param>
        /// <returns>Updated Movie in Swagger</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, MovieEditDTO movieDTO)
        {
            if (id != movieDTO.MovieId)
            {
                return BadRequest();
            }
            if (!_movieService.MovieExist(id))
            {
                return NotFound();
            }
            Movie modelMovie = _mapper.Map<Movie>(movieDTO);
            await _movieService.UpdateMovie(modelMovie);

            return NoContent();
        }

        
        
        /// <summary>
        /// Methode to add a new Movie in Swagger
        /// </summary>
        /// <param name="movieDTO"></param>
        /// <returns>Adding a new Movie in Swagger</returns>
        [HttpPost]
        public async Task<ActionResult<Movie>> PostMovie(MovieCreateDTO movieDTO)
        {
            Movie modelMovie = _mapper.Map<Movie>(movieDTO);
            modelMovie = await _movieService.AddMovie(modelMovie);
            return CreatedAtAction("GetMovies",
                new { id = modelMovie.MovieId },
                _mapper.Map<MovieReadDTO>(modelMovie));
        }

        
        /// <summary>
        /// Delete a movie by Id in Swagger
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Deletes a movie in Swagger</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            if (!_movieService.MovieExist(id))
            {
                return NotFound();
            }
            await _movieService.DeleteMovie(id);
            return NoContent();
        }
        
        /// <summary>
        /// Get all Characters in Movie by id in Swagger
        /// </summary>
        /// <param name="id"></param>
        /// <returns>All Characters</returns>
        [HttpGet("GetCharacter/{id}")]
        public async Task<ActionResult<List<MovieCharacterDTO>>> GetCharactersInMovie(int id)
        {
            List<Movie> Movie = await _movieService.CharactersInMovie(id);

            if (Movie == null)
            {
                return NotFound();
            }

            var movieReadCharacters = _mapper.Map<List<MovieCharacterDTO>>(Movie);
            return movieReadCharacters;
        }

        /// <summary>
        /// Update a Character Id in a spesific Movie by Id Swagger 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="characters"></param>
        /// <returns>Updated Character</returns>
        [HttpPut("GetCharacters/{id}")]
        public async Task<IActionResult> UpdateMovieCharacters(int id, List<int> characters)
        {
            if (!_movieService.MovieExist(id))
            {
                return NotFound();
            }
            try
            {
                await _movieService.UpdateMovieCharacters(id, characters);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Invalid character");
            }
            return NoContent();
        }
    }
}
