﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using oblig3.Data;
using oblig3.DataTransferObject.CharacterDTO;
using oblig3.DataTransferObject.MovieDTO;
using oblig3.Model;
using oblig3.Services;

namespace oblig3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("Application/json")]
    [Consumes("Application/json")]
    public class CharactersController : ControllerBase
    {
        private readonly ICharacterService _characterService;
        private readonly IMapper _mapper;

        public CharactersController(ICharacterService characterService, IMapper mapper)
        {
            _characterService = characterService;
            _mapper = mapper;
        }
        
        /// <summary>
        /// Method to get all characters
        /// </summary>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        
        
        [HttpGet]
        
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacters()
        {
            return _mapper.Map<List<CharacterReadDTO>>(await _characterService.GetAllCharacters());
        }
        
        
        /// <summary>
        /// Method to get a spesific Character by id in Swagger
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A spesific Character in Swagger</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacter(int id)
        {

            Character character = await _characterService.GetCharacterById(id);

            if (character == null)
            {
                return NotFound();
            }

            return _mapper.Map<CharacterReadDTO>(character);
        }


        /// <summary>
        /// Methode to Update a spesific Characther by Id in Swagger
        /// </summary>
        /// <param name="id"></param>
        /// <param name="characterDTO"></param>
        /// <returns>Updated Characther in Swagger</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, CharacterEditDTO characterDTO)
        {
            if (id != characterDTO.CharacterId)
            {
                return BadRequest();
            }

            if (!_characterService.CharacterExist(id))
            {
                return NotFound();
            }

            Character modelCharacter = _mapper.Map<Character>(characterDTO);
            await _characterService.UpdateCharacter(modelCharacter);

            return NoContent();
        }

        /// <summary>
        /// Post/Creates a character in Swagger
        /// </summary>
        /// <param name="characterDTO"></param>
        /// <returns>a newly created character in Swagger</returns>
        [HttpPost]
        public async Task<ActionResult<Character>> PostCharacter(CharacterCreateDTO characterDTO)
        {
            Character modelCharacter = _mapper.Map<Character>(characterDTO);
            modelCharacter = await _characterService.AddCharacter(modelCharacter);
            return CreatedAtAction("GetCharacter",
                new { id = modelCharacter.CharacterId },
                _mapper.Map<CharacterReadDTO>(modelCharacter));
        }

        /// <summary>
        /// Methode to delete a characther in Swagger
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Deleted Characther in Swagger</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            if (!_characterService.CharacterExist(id))
            {
                return NotFound();
            }
            await _characterService.DeleteCharacter(id);

            return NoContent();
        }
    }
}
