# Entity Framework Core Code First
## Description
In this assignment we created an Entity Framework code first workflow and an ASP.NET Core Web API in C#.
This assignment was divided into 2 parts:

### Part 1
#### EF Core Code First
In this part of the assignment we  created a database made in SQL Server through EF Core with a RESTful API.
In this database we stored information about multiple things such as characters, movies, franchise.
We specified relationship between some of the entities such as Character and movies (many to many) and between
movies and franchises (one to many). We created some seeded data for testing purposes, and installed/created a 
screenshot of the database diagram which is in this repository.

### Part 2
In this part of the assignment we created services and service interfaces that cointains CRUD methods.
We created controllers so that we could interact with the created methods in swagger.
We used automapping in this assignment and created DTOs of all our models.
We created profiles to specify the correct mapping between the dto's and the models.

In the end we created a CI pipeline to build our application as a docker artifact.

### Technologies
* C#
* .NET6
* EF Core
* ASP.NET
* Automapper
* Microsoft SQL Server Management Studio

## Contributors
### Jarand & Kjetil